Feature('Favorite Restaurant');

Before(({ I }) => {
  I.amOnPage('/#/favorites');
});

const assert = require('assert');

Scenario('showing empty favorite restaurant',  ({ I }) => {
  I.seeElement('.emptyState');
  I.see('No Restaurant Found', '.emptyState h2');
});

Scenario('add a restaurant to favorite', async ({ I }) => {
  I.see('No Restaurant Found', '.emptyState h2');

  I.amOnPage('/');

  I.seeElement('card-elem');
  const firstRestaurant = locate('card-elem').first();
  const firstRestaurantName = await I.grabTextFrom(firstRestaurant.find('.cardTitle'));
  I.click(firstRestaurant.find('a'));

  I.seeElement('favorite-button');
  I.click('favorite-button');

  I.amOnPage('/#/favorites');

  I.seeElement('card-elem');
  const favoritedRestaurantName = await I.grabTextFrom('card-elem .cardTitle');

  assert.strictEqual(firstRestaurantName, favoritedRestaurantName);
});

Scenario('remove a restaurant from favorite', async ({ I }) => {
  I.see('No Restaurant Found', '.emptyState h2');

  I.amOnPage('/');

  I.seeElement('card-elem');
  const firstRestaurant = locate('card-elem').first();
  const firstRestaurantName = await I.grabTextFrom(firstRestaurant.find('.cardTitle'));
  I.click(firstRestaurant.find('a'));

  I.seeElement('favorite-button');
  I.click('favorite-button');

  I.amOnPage('/#/favorites');

  I.seeElement('card-elem');
  const favoritedRestaurantName = await I.grabTextFrom('card-elem .cardTitle');

  assert.strictEqual(firstRestaurantName, favoritedRestaurantName);

  I.click('card-elem a');

  I.seeElement('favorite-button');
  I.click('favorite-button');

  I.amOnPage('/#/favorites');

  I.seeElement('.emptyState');
  I.see('No Restaurant Found', '.emptyState h2');
});

import CONFIG from '../globals/config';

class CardElem extends HTMLElement {
  set restaurant(restaurant) {
    this.classList = 'block card';
    this.tabIndex = 0;
    this._restaurant = restaurant;
    this.render();
  }

  render() {
    this.id = this._restaurant.id;
    this.innerHTML = `
      <div class="cardHeader">
        <div class="cardImage">
          <img class="lazyload" data-src="${CONFIG.BASE_SMALL_IMAGE_URL}/${this._restaurant.pictureId}" alt="view of ${this._restaurant.name} restaurant" />  <!-- Decorative Image -->
        </div>
      </div>
      <div class="cardBody">
        <h3 class="cardTitle" role="heading">
          ${this._restaurant.name}
        </h3>
        <div class="cardInfo">
          <div class="cardRating">
            <!-- Star Icon -->
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
              stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <polygon
                points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2">
              </polygon>
            </svg>
            <p aria-label="rating ${this._restaurant.rating}">${this._restaurant.rating}</p>
          </div>
          <div class="cardCity">
            <!-- Pin Icon -->
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
              stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
              <circle fill="white" stroke="white" cx="12" cy="10" r="3"></circle>
            </svg>
            <p aria-label="in ${this._restaurant.city} city">${this._restaurant.city}</p>
          </div>
        </div>
        <div class="cardAction">
          <a href="/#/detail/${this._restaurant.id}" type="link">View Details</a>
        </div>
      </div>
    `;
  }
}

customElements.define('card-elem', CardElem);

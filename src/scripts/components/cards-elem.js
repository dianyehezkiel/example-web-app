import './card-elem';

class CardsElem extends HTMLElement {
  set restaurants(restaurants) {
    this.classList = 'block cards';
    this.restaurantsData = restaurants;
    this.render();
  }

  render() {
    this.innerHTML = '';
    this.restaurantsData.forEach((restaurant) => {
      const card = document.createElement('card-elem');
      card.restaurant = restaurant;
      this.appendChild(card);
    });
  }
}

customElements.define('cards-elem', CardsElem);

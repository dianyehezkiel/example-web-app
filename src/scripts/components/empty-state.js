class EmptyState extends HTMLElement {
  set config({ message }) {
    this.classList = 'block container emptyState';
    this._message = message;
    this.render();
  }

  render() {
    this.innerHTML = `
      <div>
        <h2>No Restaurant Found</h2>
        <p>${this._message}</p>
      </div>
    `;
  }
}

customElements.define('empty-state', EmptyState);

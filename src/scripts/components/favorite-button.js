class FavoriteButton extends HTMLElement {
  set config({ favorited = false }) {
    this._favorited = favorited;
    this.render();
  }

  render() {
    this.innerHTML = `
      <button aria-label="${this._favorited ? 'remove from favorite restaurant' : 'add to favorite restaurant'}" id="favoriteButton" class="favoriteButton">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="${this._favorited ? 'red' : 'white'}" stroke-linecap="round" stroke-linejoin="round">
          <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z"/>
        </svg>
      </button>
    `;
  }
}

customElements.define('favorite-button', FavoriteButton);

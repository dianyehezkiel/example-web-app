import CONFIG from '../globals/config';

class RestaurantDetailElem extends HTMLElement {
  set config({ restaurant }) {
    this.classList = 'block container restaurantList';
    this._restaurant = restaurant;
    this.render();
  }

  render() {
    this.innerHTML = `
      <div id="detail">
        <div id="detailHeader">
          <div id="detailImage">
            <img src="${CONFIG.BASE_LARGE_IMAGE_URL}/${this._restaurant.pictureId}" alt="view of ${this._restaurant.name} restaurant" />  <!-- Decorative Image -->
          </div>
          <div id="detailInfo">
            <div>
              <h2 role="heading" class="lineClamp">${this._restaurant.name}</h2>
              <div id="favoriteButtonContainer"></div>
            </div>
            <div>
              <div class="detailRating">
                <!-- Star Icon -->
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <polygon
                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2">
                  </polygon>
                </svg>
                <p aria-label="rating ${this._restaurant.rating}">${this._restaurant.rating}</p>
              </div>
              <div class="detailCity">
                <!-- Pin Icon -->
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z"></path>
                  <circle fill="white" stroke="white" cx="12" cy="10" r="3"></circle>
                </svg>
                <p aria-label="in ${this._restaurant.city} city">${this._restaurant.city}</p>
              </div>
            </div>
          </div>
        </div>
        <div id="detailBody">
          <p>${this._restaurant.description}</p>
          <p><b>Categories:</b> ${this._restaurant.categories.map((category) => category.name).join(', ')}</p>
          <p><b>Address:</b> ${this._restaurant.address}, ${this._restaurant.city}</p>
          <h3><b>Menus:</b></h3>
          <div id="detailMenus">
            <div id="detailFoods">
              <h4>Foods</h4>
              <ul>
                ${this._restaurant.menus.foods.map((food) => `<li>${food.name}</li>`).join('')}
              </ul>
            </div>
            <div id="detailDrinks">
              <h4>Drinks</h4>
              <ul>
                ${this._restaurant.menus.drinks.map((drink) => `<li>${drink.name}</li>`).join('')}
              </ul>
            </div>
          </div>

          <h3><b>Customer Reviews:</b></h3>
          <div id="detailReviews">
            ${this._restaurant.customerReviews.map((review) => `
              <div class="detailReview">
                <div class="detailReviewHeader">
                  <h4>${review.name}</h4>
                  <i>${review.date}</i>
                </div>
                <p>${review.review}</p>
              </div>
            `).join('')}
          </div>
        </div>
      </div>
    `;
  }

  getFavoriteButtonContainer() {
    return this.querySelector('#favoriteButtonContainer');
  }
}

customElements.define('restaurant-detail', RestaurantDetailElem);

import './cards-elem';

class RestaurantsElem extends HTMLElement {
  set config({ restaurants, title = 'Recommended Restaurants' }) {
    this.classList = 'block container restaurantList';
    this._title = title;
    this._restaurants = restaurants;
    this.render();
  }

  render() {
    this.innerHTML = `
      <h2 class="title" role="heading">${this._title}<h2>
    `;
    const cards = document.createElement('cards-elem');
    cards.restaurants = this._restaurants;
    this.appendChild(cards);
  }
}

customElements.define('restaurants-elem', RestaurantsElem);

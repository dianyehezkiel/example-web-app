import API_ENDPOINTS from '../globals/api-endpoints';

class RestaurantApiSource {
  static async allRestaurants() {
    const response = await fetch(API_ENDPOINTS.GET_ALL);
    const responseJson = await response.json();
    return responseJson.restaurants;
  }

  static async detailRestaurant(id) {
    const response = await fetch(API_ENDPOINTS.GET_DETAIL(id));
    const responseJson = await response.json();
    return responseJson.restaurant;
  }

  static async searchRestaurants(query) {
    const response = await fetch(API_ENDPOINTS.SEARCH(query));
    const responseJson = await response.json();
    return responseJson.restaurants;
  }

  static async reviewRestaurant({
    id,
    name,
    review,
  }) {
    const response = await fetch(API_ENDPOINTS.POST_REVIEW, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        name,
        review,
      }),
    });
    const responseJson = await response.json();
    return responseJson.customerReviews;
  }
}

export default RestaurantApiSource;

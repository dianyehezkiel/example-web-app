import CONFIG from './config';

const API_ENDPOINTS = {
  GET_ALL: `${CONFIG.BASE_URL}/list`,
  GET_DETAIL: (restaurantId) => `${CONFIG.BASE_URL}/detail/${restaurantId}`,
  SEARCH: (query) => `${CONFIG.BASE_URL}/search?q=${query}`,
  POST_REVIEW: `${CONFIG.BASE_URL}/review`,
};

export default API_ENDPOINTS;

import 'regenerator-runtime'; /* for async await transpile */
import 'lazysizes';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import '../styles/style.scss';
import App from './views/app';

const app = new App({
  openButton: document.getElementById('openDrawer'),
  closeButton: document.getElementById('closeDrawer'),
  drawer: document.getElementById('drawer'),
  content: document.getElementById('main'),
  toggleTabIndexElems: document.querySelectorAll('.toggleTabIndex'),
  nav: document.getElementById('nav'),
});

window.addEventListener('hashchange', () => {
  app.renderPage();
});

window.addEventListener('load', () => {
  app.renderPage();
});

const Contact = {
  render() {
    return `
      <div id="mainContent" class="contact" tabindex="0">
        <div class="overlay">
          <div class="container">
            <h2>Help Us Improve</h2>
            <div class="flexContact">
              <div>
                <p>Have any feedback or suggestion?</p>
                <p>Feel free to contact us!</p>
              </div>
              <form action="https://formspree.io/f/meqwdnlg" method="POST">
                <label>
                  Your email:
                  <input type="email" name="email">
                </label>
                <label>
                  Your message:
                  <textarea name="message"></textarea>
                </label>
                <button type="submit">Send Message</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    `;
  },

  afterRender() {},
};

export default Contact;

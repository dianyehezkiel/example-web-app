import RestaurantApiSource from '../data/restaurant-api-source';
import UrlParser from '../routes/url-parser';
import '../components/restaurant-detail';
import FavButtonPresenter from '../utils/fav-button-presenter';

const Detail = {
  async render() {
    return `
      <div id="mainContent" class="detail" tabindex="0"></div>
    `;
  },

  async afterRender() {
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    const restaurant = await RestaurantApiSource.detailRestaurant(url.id);
    const restaurantDetailElem = document.createElement('restaurant-detail');
    restaurantDetailElem.config = { restaurant };

    const favButtonContainer = restaurantDetailElem.getFavoriteButtonContainer();

    FavButtonPresenter.init({
      favButtonContainer,
      restaurant: {
        id: restaurant.id,
        name: restaurant.name,
        rating: restaurant.rating,
        city: restaurant.city,
        pictureId: restaurant.pictureId,
      },
    });

    const mainContent = document.getElementById('mainContent');
    mainContent.appendChild(restaurantDetailElem);
  },
};

export default Detail;

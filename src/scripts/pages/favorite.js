import FavoriteRestaurantIdb from '../data/favorite-restaurant-idb';
import '../components/restaurants-elem';
import '../components/empty-state';

const Favorites = {
  async render() {
    return `
      <div id="mainContent" class="favorites" tabindex="0"></div>
    `;
  },

  async afterRender() {
    const restaurants = await FavoriteRestaurantIdb.getAllRestaurants();

    const mainContent = document.getElementById('mainContent');
    if (!restaurants?.length) {
      const emptyState = document.createElement('empty-state');
      emptyState.config = { message: 'You don\'t have any favorite restaurants yet' };

      mainContent.appendChild(emptyState);
      return;
    }

    const restaurantsElem = document.createElement('restaurants-elem');
    restaurantsElem.config = { restaurants, title: 'Favorite Restaurants' };

    mainContent.appendChild(restaurantsElem);
  },
};

export default Favorites;

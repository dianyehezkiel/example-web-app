import RestaurantApiSource from '../data/restaurant-api-source';
import '../components/restaurants-elem';

const Home = {
  async render() {
    return `
      <header>
        <div class="container">
          <div class="heroImage">
            <picture>
              <source media="(max-width: 768px)" srcset="./images/heros/hero-image_2-small.jpg">
              <img src="./images/heros/hero-image_2-large.jpg" alt="" /> <!-- Decorative Image -->
            </picture>
          </div>
          <div class="heroContent">
            <h1>My Restaurant</h1>
            <p>Find your favorite restaurant here</p>
          </div>
      </header>
      <div id="mainContent" class="home" tabindex="0"></div>
    `;
  },

  async afterRender() {
    const restaurants = await RestaurantApiSource.allRestaurants();
    const mainContent = document.getElementById('mainContent');

    if (!restaurants?.length) {
      const emptyState = document.createElement('empty-state');
      emptyState.config = { message: 'There\'s no restaurant to show' };

      mainContent.appendChild(emptyState);
      return;
    }

    const restaurantsElem = document.createElement('restaurants-elem');
    restaurantsElem.config = { restaurants };

    mainContent.appendChild(restaurantsElem);
  },
};

export default Home;

const routeToMenu = {
  '/': 'home',
  '/favorites': 'favorites',
  '/contact': 'contact',
};

export default routeToMenu;

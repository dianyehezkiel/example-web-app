import Contact from '../pages/contact';
import Detail from '../pages/detail';
import Favorites from '../pages/favorite';
import Home from '../pages/home';

const routes = {
  '/': Home, // default page
  '/favorites': Favorites,
  '/contact': Contact,
  '/detail/:id': Detail,
};

export default routes;

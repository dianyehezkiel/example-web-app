const DrawerInitiator = {
  init({
    openButton,
    closeButton,
    drawer,
    content,
    toggleTabIndexElems,
  }) {
    openButton.addEventListener('click', (event) => {
      this._openDrawer(event, drawer, toggleTabIndexElems);
    });

    closeButton.addEventListener('click', (event) => {
      this._closeDrawer(event, drawer, toggleTabIndexElems);
    });

    content.addEventListener('click', (event) => {
      this._closeDrawer(event, drawer, toggleTabIndexElems);
    });
  },

  _openDrawer(event, drawer, toggleTabIndexElems) {
    event.stopPropagation();
    drawer.classList.add('show');
    drawer.classList.remove('hide');
    toggleTabIndexElems.forEach((elem) => {
      elem.setAttribute('tabindex', '0');
    });
  },

  _closeDrawer(event, drawer, toggleTabIndexElems) {
    event.stopPropagation();
    drawer.classList.remove('show');
    drawer.classList.add('hide');
    toggleTabIndexElems.forEach((elem) => {
      elem.setAttribute('tabindex', '-1');
    });
  },
};

export default DrawerInitiator;

import FavoriteRestaurantIdb from '../data/favorite-restaurant-idb';
import '../components/favorite-button';

const FavButtonPresenter = {
  async init({ favButtonContainer, restaurant }) {
    this._favButtonContainer = favButtonContainer;
    this._restaurant = restaurant;

    await this._renderButton();
  },

  async _renderButton() {
    const { id } = this._restaurant;

    if (await this._isRestaurantExist(id)) {
      this._renderFavorited();
    } else {
      this._renderFavorite();
    }
  },

  async _isRestaurantExist(id) {
    const restaurant = await FavoriteRestaurantIdb.getRestaurant(id);
    return !!restaurant;
  },

  _renderFavorite() {
    const favoriteButton = document.createElement('favorite-button');
    favoriteButton.config = { favorited: false };

    favoriteButton.addEventListener('click', async () => {
      await FavoriteRestaurantIdb.putRestaurant(this._restaurant);
      this._renderButton();
    });

    this._favButtonContainer.innerHTML = '';
    this._favButtonContainer.appendChild(favoriteButton);
  },

  _renderFavorited() {
    const favoriteButton = document.createElement('favorite-button');
    favoriteButton.config = { favorited: true };

    favoriteButton.addEventListener('click', async () => {
      await FavoriteRestaurantIdb.deleteRestaurant(this._restaurant.id);
      this._renderButton();
    });

    this._favButtonContainer.innerHTML = '';
    this._favButtonContainer.appendChild(favoriteButton);
  },
};

export default FavButtonPresenter;

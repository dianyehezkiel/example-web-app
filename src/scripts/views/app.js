import routes from '../routes/routes';
import routeToMenu from '../routes/routes-menu';
import UrlParser from '../routes/url-parser';
import DrawerInitiator from '../utils/drawer-initiator';

class App {
  constructor({
    openButton,
    closeButton,
    drawer,
    content,
    toggleTabIndexElems,
    nav,
  }) {
    this._openButton = openButton;
    this._closeButton = closeButton;
    this._drawer = drawer;
    this._content = content;
    this._toggleTabIndexElems = toggleTabIndexElems;
    this._nav = nav;
    this._initialAppShell();
  }

  _initialAppShell() {
    DrawerInitiator.init({
      openButton: this._openButton,
      closeButton: this._closeButton,
      drawer: this._drawer,
      content: this._content,
      toggleTabIndexElems: this._toggleTabIndexElems,
    });
  }

  async renderPage() {
    const url = UrlParser.parseActiveUrlWithCombiner();

    this._nav.querySelectorAll('.menu').forEach((navLink) => {
      navLink.classList.remove('active');
      if (navLink.innerText.toLowerCase() === routeToMenu[url]) {
        navLink.classList.add('active');
      }
    });

    const page = routes[url];
    this._content.innerHTML = await page.render();
    await page.afterRender();
    const skipLinkElem = document.querySelector('.skip-link');
    skipLinkElem.addEventListener('click', (event) => {
      event.preventDefault();
      document.querySelector('#mainContent').focus();
    });
  }
}

export default App;
